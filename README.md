
### Steps to run the framework 

- Install JDK 11 
- Install IDE (preferred IntelliJ IDEA)
- Install Appium : [link](https://support.smartbear.com/testcomplete/docs/app-testing/mobile/device-cloud/configure-appium/index.html)
  
* Can run locally by cloning the repo
   *  For API test can import to Postman and run the collection.
   *  For mobile test can run in IntelliJ by running the `testng-mobile.xml` or can run in terminal `mvn test mobile/testng-mobile.xml` 
    

